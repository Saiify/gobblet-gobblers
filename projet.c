#include <stdio.h>
#include <stdlib.h>
#include "board.h"
 
void regles(){
    printf("Le jeu se joue à deux joueurs sur un plateau de trois cases sur trois,\nbien que des variantes puissent être jouées sur des plateaux plus grands.\nChaque joueur a le contrôle d'un ensemble de six pièces dont le propriétaire est facilement identifiable,\npar exemple par la forme ou la couleur de la pièce.\nLes pièces sont de trois tailles différentes (grande, moyenne, petite),\nchaque joueur ayant deux de chaque taille.\n\n");
    printf("Au cours de la partie, les joueurs effectuent alternativement un coup qui consiste en l'un ou l'autre :\n-En ajoutant une pièce de leur collection sur le tableau.\n-En déplaçant une de ses pièces visibles déjà sur le plateau vers une autre case.\n-Une pièce peut être placée soit sur une case vide,\n");
    printf("soit sur une case contenant uniquement des pièces plus petites,\nindépendamment du propriétaire de la pièce.\n\nDans ce dernier cas, la pièce recouvre et cache les plus petites pièces.\nLorsqu'une pièce placée au-dessus d'autres pièces est déplacée d'une case,\nla plus grande pièce couverte est révélée et peut être jouée dans un tour ultérieur.\n");
    printf("La partie est gagnée par le premier joueur qui parvient à faire une ligne visible de ses pièces\nà travers le plateau (sur une ligne, une colonne, ou une diagonale, comme pour le Tic-Tac-Toe).\n\n______________________\n\n");
}
 
void viderBuffer()
{
    int c = 0;
    while (c != '\n' && c != EOF)
    {
        c = getchar();
    }
}
 
void tour_joueur(player current_player){
    if(current_player == 1){
        printf("\033[1;32m");
        printf("Joueur Numéro %d, à toi de jouer !\n\n", current_player);
    }
 
    if(current_player == 2){
        printf("\033[1;34m");
        printf("Joueur Numéro %d, à vous de jouer !\n\n", current_player);
    }
    printf("\033[0m");
}
 
void affiche_jeu(board game){
 
    printf("\n\n\n");
    for(int i=0;i<3;i++){
        printf("\n");
        if(i == 0)printf("┌───┬───┬───┐\n");
        else(printf("├───┼───┼───┤\n"));
        printf("│ ");
        for(int j=0;j<3;j++){
            printf("\033[1;34m");
            if(get_place_holder(game,i,j)==2){
                if(get_piece_size(game,i,j)==0){
                    printf(" ");
                }
                if(get_piece_size(game,i,j)==1){
            char a[3]="•";
                    printf("%s",a);
                }
                if(get_piece_size(game,i,j)==2){
                    printf("o");
                }
                if(get_piece_size(game,i,j)==3){
                    printf("0");
                }
            }
            else{
                printf("\033[1;32m");
                if(get_piece_size(game,i,j)==0){
                    printf(" ");
                }
                if(get_piece_size(game,i,j)==1){
                    printf("*");
                }
                if(get_piece_size(game,i,j)==2){
                    printf("x");
                }
                if(get_piece_size(game,i,j)==3){
                    printf("X");
                }
            }
            printf("\033[0m");
            printf(" │ ");
         
        }  
    }
    printf("\n└───┴───┴───┘\n");
 
 
}
 
 
int condition_ajout_piece(board game, int line, int column,size piece_size, player current_player){
    int resultat=0;
    if(piece_size > get_piece_size(game,line,column )){
        resultat=1;
    }else{
        if(get_nb_piece_in_house(game,current_player,piece_size) == 0){
            printf("\n Vous n'avez plus de pièce de taille %d", piece_size);
        }else{
            printf("\n\nUne pièce de même taille où de taille supérieur se trouve déjà sur cette case\n\n");
        }
    }
    return resultat;
}
 
int condition_deplacer_piece(board game, int line, int column,size piece_size, player current_player){
    int resultat=0;
	if(get_piece_size(game,line,column) != 0){
		if(piece_size > get_piece_size(game,line,column )){
			resultat=1;
		}else{
			printf("\n\nUne pièce de même taille où de taille inférieur se trouve déjà sur cette case :");
		}
	}
	else{
		resultat=1;
	}
    return resultat;
}
 
 
 
void affiche_maison(board game, player current_player){
    int size=0;
    printf("Il vous reste");
    for(size=1;size<4;size++){
        if (size==1){
            printf(" %d petite(s) pièce(s),",get_nb_piece_in_house(game, current_player,size));
        }
        if(size==2){
            printf(" %d moyenne(s) pièce(s) et",get_nb_piece_in_house(game, current_player,size));
        }
        if(size==3){
            printf(" %d grande(s) pièce(s).\n",get_nb_piece_in_house(game, current_player,size));
        }
    }
    printf("\n");
}
 
void accueil(){
    printf("┌───────────────────────────────────────────────────────────┐\n");
    printf("│▁ ▂ ▄ ▅ ▆ ▇ █ ⓖ ⓞ ⓑ ⓑ ⓛ ⓔ ⓣ  ⓖ ⓞ ⓑ ⓑ ⓛ ⓔ ⓡ ⓢ █ ▇ ▆ ▅ ▄ ▂ ▁ │\n");
    printf("└───────────────────────────────────────────────────────────┘\n");
    printf("\n\n");
    printf("Bienvenue sur le jeu Gobblet Gobblers ! \nDéveloppé par Félix Lafontaine et Hugo Mercier\n\n\n");
 
}
 
void quitter(){
    int color=1;
    printf("┌");
    for(int k=0;k<11;k++){
        if(color%2==1){
            printf("\033[1;31m");
        }
        else{
            printf("\033[0m");
        }
        printf("─");
        color++;
    }
    printf("\033[0m");
    printf("┐\n│");
    printf(" À bientot ");
    printf("\033[1;31m");
    printf("│\n└");
    for(int k=0;k<11;k++){
        if(color%2==1){
            printf("\033[1;31m");
        }
        else{
            printf("\033[0m");
        }
        printf("─");
        color++;
    }
    printf("┘\n");
}
 
 
int main(){
    system("clear");
   
    player current_player=1;
    int choix;
    accueil();
    do{
        board game =new_game();
        printf("Faites un choix :\n\n");
        printf("\033[1;31m");
        printf("┌────────────────────┐\n│");
        printf("\033[0m");
        printf("  1-Nouvelle partie ");
        printf("\033[1;31m");
        printf("│\n└────────────────────┘\n");
        printf("\033[0m"); //encadrage en rouge
        printf("\033[1;35m");
        printf("┌────────────────────┐\n│");
        printf("\033[0m");
        printf("      2-Règles      ");
        printf("\033[1;35m");
        printf("│\n└────────────────────┘\n");
        printf("\033[0m"); //encadrage en violet
        printf("\033[1;33m");
        printf("┌────────────────────┐\n│");
        printf("\033[0m");
        printf("     3-Quitter      ");
        printf("\033[1;33m");
        printf("│\n└────────────────────┘\n");
        printf("\033[0m"); //encadrage en jaune
        do{
            printf("\nChoix : ");
            scanf(" %d",&choix);
            printf("\n");
            printf("\033[1;31m");
            printf("───────────────────────────────────\n\n");
            printf("\033[0m");
            if(choix<1 || choix>3){
                printf("Mauvaise réponse\n");
            }
	    viderBuffer();
        }while(choix!=1 && choix!=2 && choix!=3);
        switch (choix){
            case 1:
                printf("Un nouveau plateau a été créé.\n\n");
                printf("┌───┬───┬───┐\n│   │   │   │\n├───┼───┼───┤\n│   │   │   │\n├───┼───┼───┤\n│   │   │   │\n└───┴───┴───┘\n\n");    
                printf("Les joueurs ont chacun 2 petites pièces, 2 moyennes et 2 grandes.\n\n");
                while(get_winner(game) == 0){
                    tour_joueur(current_player);
                    affiche_maison(game, current_player);
		            printf("Voulez-vous ajouter [1] ou déplacer une pièce [2] ?\nChoix : ");
		            int choix2;
		            do{
		                scanf("%d",&choix2);
		                if(choix2!=1 && choix2!=2){
		                    printf("Mauvaise réponse !\nChoix : ");
		                }
	 		viderBuffer();	
                    }while(choix2!=1 && choix2!=2);
                    if(choix2==2){
                        int ligneDepart;
                        int colonneDepart;
                        int ligneArrivee;
                        int colonneArrivee;
                        int resultat;
                        do{
                            printf("Quelle est la ligne de la pièce à déplacer ?\nChoix : ");
                            do{
                                scanf("%d",&ligneDepart);
                                if(ligneDepart<1 || ligneDepart>3){
                                    printf("Mauvaise réponse !\nChoix :");
                                }
				viderBuffer();
                            }while(ligneDepart<1 || ligneDepart>3);
                            ligneDepart--;
                            printf("Quelle est la colonne de la pièce à déplacer ?\nChoix : ");
                            do{                        
                                scanf("%u",&colonneDepart);
                                if(colonneDepart<1 || colonneDepart>3){
                                    printf("Mauvaise réponse !\nChoix : ");
                                }
                            }while(colonneDepart<1 || colonneDepart>3);
                            colonneDepart--;
                            if(get_place_holder(game,ligneDepart, colonneDepart) != current_player){
                                printf("Mauvaise réponse !\nChoix : ");
                            }
			    viderBuffer();
                        }while(get_place_holder(game,ligneDepart, colonneDepart) != current_player);
                        do{
                            printf("Sur quelle ligne voulez-vous la déplacer ?\nChoix : ");
                            do{
                                scanf("%d",&ligneArrivee);
                                if(ligneArrivee<1 || ligneArrivee>3){
                                    printf("Mauvaise réponse !\nChoix : ");
                                }
				viderBuffer();
                            }while(ligneArrivee<1 || ligneArrivee>3);
                            ligneArrivee--;
                            printf("Sur quelle colonne voulez-vous la déplacer ?\nChoix : ");
                            do{
                                scanf("%d",&colonneArrivee);
                                if(colonneArrivee<1 || colonneArrivee>3){
                                    printf("Mauvaise réponse !\nChoix : ");
                                }
                            }while(colonneArrivee<1 || colonneArrivee>3);
                            colonneArrivee--;
			    size piece = get_piece_size(game, ligneDepart, colonneDepart);
                            resultat=condition_deplacer_piece(game, ligneArrivee, colonneArrivee,piece ,current_player);
                        }while(resultat !=1);  
                            move_piece(game ,ligneDepart, colonneDepart, ligneArrivee, colonneArrivee);
			    
                    }else{
                        int resultat;
                        size tailleP;
                        int ligneP;
                        int colonneP;
                        do{
                            printf("Quelle ligne ?\nChoix : ");
                            do{
                                scanf("%d",&ligneP);
                                if(ligneP<1 || ligneP>3){
                                    printf("Mauvaise réponse !\nChoix : ");
                                }
				viderBuffer();
                            }while(ligneP<1 || ligneP>3);
                            ligneP--;
                            printf("Quelle colonne ?\nChoix : ");
                            do{
                                scanf("%u",&colonneP);
                                if(colonneP<1 || colonneP>3){
                                    printf("Mauvaise réponse !\nChoix : ");
                                }
				viderBuffer();
                            }while(colonneP<1 || colonneP>3);  
                            colonneP--;
                            printf("Quelle taille ? [1] petit, [2] moyen, [3] grand\nChoix : ");
                            do{
                                scanf("%u",&tailleP);
                                if(tailleP<1 || tailleP>3){
                                    printf("Mauvaise réponse !\nChoix : ");   
                                }
				viderBuffer();
                            }while(tailleP<1 || tailleP>3);
                            resultat = condition_ajout_piece(game, ligneP, colonneP, tailleP, current_player);
                        }while(resultat != 1);
                        place_piece(game, current_player, tailleP, ligneP, colonneP);
                        resultat=0;
                    }
                    affiche_jeu(game);
                    current_player = next_player(current_player);
                }
                printf("Le joueur %d a perdu, dommage !\n",current_player );
                printf("La partie est terminée.\n\n");
                printf("\033[1;31m");
                printf("______________________\n\n");
               
                break;
            case 2:
                regles();
                break;
            case 3:
                quitter();
                break;
        }
     destroy_game(game);
    }while(choix!=3);
    return 0;
}

