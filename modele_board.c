#include "board.h"

/**
 * \file board.c
 *
 * \brief Source code associated with \ref board.h
 *
 * \author You?
 */

/**
 * @brief The board of the game, define it as you wish.
 */
struct board_s {
	// TODO: compléter la structure.
};

board new_game(){ 
	// memory allocation for the board (leave it as is)
	board new_board = malloc(sizeof(struct board_s));
	// TODO: Insérer les traitements que vous voudriez faire.
	return new_board;
}

void destroy_game(board game){
	free(game);
}
